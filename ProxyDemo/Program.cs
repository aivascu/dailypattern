﻿using ProxyDemo.Proxy;
using System;

namespace ProxyDemo
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            OrcNameGeneratorProxy proxy = new OrcNameGeneratorProxy();
            Console.WriteLine("Data from Proxy Client = {0}", proxy.Generate());

            Console.ReadKey();
        }
    }
}
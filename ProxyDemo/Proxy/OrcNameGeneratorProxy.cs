﻿using ProxyDemo.Generators;
using System;

namespace ProxyDemo.Proxy
{
    public class OrcNameGeneratorProxy : INameGenerator
    {
        private OrcNameGenerator client;

        public OrcNameGeneratorProxy()
        {
            Console.WriteLine("ProxyClient: Initialized");
        }

        public string Generate()
        {
            if (client == null)
                client = new OrcNameGenerator();

            return client.Generate();
        }
    }
}
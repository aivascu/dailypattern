﻿namespace ProxyDemo.Generators
{
    internal interface INameGenerator
    {
        string Generate();
    }
}
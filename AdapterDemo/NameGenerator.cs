﻿namespace AdapterDemo
{
    public class NameGenerator
    {
        private INameGeneratorAdapter nameGenerator;

        public NameGenerator(INameGeneratorAdapter nameGenerator)
        {
            this.nameGenerator = nameGenerator;
        }

        public void ChangeGenerator(INameGeneratorAdapter nameGenerator)
        {
            this.nameGenerator = nameGenerator;
        }

        public string GetMaleName()
        {
            return nameGenerator.GenerateMaleName();
        }

        public string GetFemaleName()
        {
            return nameGenerator.GenerateFemaleName();
        }
    }
}
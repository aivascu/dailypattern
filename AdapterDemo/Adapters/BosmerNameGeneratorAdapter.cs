﻿using AdapterDemo.Generators;

namespace AdapterDemo.Adapters
{
    public class BosmerNameGeneratorAdapter : INameGeneratorAdapter
    {
        private BosmerNameGenerator generator;

        public BosmerNameGeneratorAdapter(BosmerNameGenerator nameGenerator)
        {
            this.generator = nameGenerator;
        }

        public string GenerateFemaleName()
        {
            generator.SetGender(Gender.Female);
            return GetName();
        }

        public string GenerateMaleName()
        {
            generator.SetGender(Gender.Male);
            return GetName();
        }

        private string GetName()
        {
            return $"{generator.GenerateFirstName()} {generator.GenerateLastName()}";
        }
    }
}
﻿using AdapterDemo.Generators;

namespace AdapterDemo.Adapters
{
    public class OrcNameGeneratorAdapter : INameGeneratorAdapter
    {
        private OrcNameGenerator generator;

        public OrcNameGeneratorAdapter(OrcNameGenerator nameGenerator)
        {
            this.generator = nameGenerator;
        }

        public string GenerateFemaleName()
        {
            generator.SetGender(Gender.Female);
            return GetName();
        }

        public string GenerateMaleName()
        {
            generator.SetGender(Gender.Male);
            return GetName();
        }

        private string GetName()
        {
            return $"{generator.GetFirstName()} {generator.GetLastNamePrefix()}{generator.GetLastName()}";
        }
    }
}
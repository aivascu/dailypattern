﻿namespace AdapterDemo
{
    public interface INameGeneratorAdapter
    {
        string GenerateMaleName();

        string GenerateFemaleName();
    }
}
﻿using AdapterDemo.Adapters;
using AdapterDemo.Generators;
using System;

namespace AdapterDemo
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            INameGeneratorAdapter orcNameGenerator = new OrcNameGeneratorAdapter(new OrcNameGenerator());
            INameGeneratorAdapter bosmerNameGenerator = new BosmerNameGeneratorAdapter(new BosmerNameGenerator());

            Console.WriteLine("Orc names:");
            var nameGenerator = new NameGenerator(orcNameGenerator);
            WriteSomeNames(nameGenerator);

            Console.WriteLine("\nBosmer names:");
            nameGenerator.ChangeGenerator(bosmerNameGenerator);
            WriteSomeNames(nameGenerator);
        }

        private static void WriteSomeNames(NameGenerator nameGenerator)
        {
            Console.WriteLine("\nFemale names:");
            for (int i = 0; i < 5; i++)
                Console.WriteLine(nameGenerator.GetFemaleName());

            Console.WriteLine("\nMale names:");
            for (int i = 0; i < 5; i++)
                Console.WriteLine(nameGenerator.GetMaleName());
        }
    }
}
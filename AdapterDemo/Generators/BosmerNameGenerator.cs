﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdapterDemo.Generators
{
    public class BosmerNameGenerator
    {
        private List<string> maleFirstNamePrefixes = new List<string> { "An", "Ara", "Ar", "Co", "Elis", "Karo", "Lego", "Li", "Pali", "Ria", "Sil", "Ta" };
        private List<string> maleFirstNameSuffixes = new List<string> { "cher", "dell", "driel", "gan", "gorn", "las", "man", "nis", "nor", "rim", "tan", "van" };

        private List<string> femaleFirstNamePrefixes = new List<string> { "Cas", "Cyl", "Des", "Lare", "Lego", "Lilis", "Min", "Phy", "Rilli", "Si", "U" };
        private List<string> femaleFirstNameSuffixes = new List<string> { "dra", "fina", "gina", "landra", "lerva", "na", "nia", "sa", "sandra", "thia" };

        private List<string> lastNamePrefixes = new List<string> { "Blue", "Fern", "Forest", "Ivy", "Moss", "Night", "Oak", "Pine", "River", "Shady", "Spring", "Willow" };
        private List<string> lastNameSuffixes = new List<string> { "brook", "dale", "hollow", "lake", "pool", "run", "shade", "sky", "thorn", "vale", "wind", "wood" };

        private Gender gender;

        private static Random random = new Random();

        public void SetGender(Gender gender)
        {
            this.gender = gender;
        }

        public string GenerateFirstName()
        {
            switch (gender)
            {
                case Gender.Male:
                    return $"{GetRandomString(maleFirstNamePrefixes)}{GetRandomString(maleFirstNameSuffixes)}";

                case Gender.Female:
                    return $"{GetRandomString(femaleFirstNamePrefixes)}{GetRandomString(femaleFirstNameSuffixes)}";

                default:
                    throw new IndexOutOfRangeException();
            }
        }

        public string GenerateLastName()
        {
            return $"{GetRandomString(lastNamePrefixes)}{GetRandomString(lastNameSuffixes)}";
        }

        private static string GetRandomString(ICollection<string> enumerable)
        {
            return enumerable.ElementAt(random.Next(enumerable.Count - 1));
        }
    }
}
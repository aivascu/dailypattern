﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdapterDemo.Generators
{
    public class OrcNameGenerator
    {
        private List<string> maleNames = new List<string> { "Abzag", "Abzrolg", "Abzug", "Agganor", "Aghurz", "Agnar", "Agrakh", "Agrobal", "Agstarg", "Arghur", "Ashzu", "Aturgh", "Azarg", "Azimbul", "Balarkh", "Balknakh", "Balmeg", "Baloth", "Balrook", "Balzag", "Bargrug", "Bashagorn", "Batgrul", "Bazrag", "Begnar", "Bekhwug", "Bhagrun", "Biknuk", "Bisquelas", "Blodrat", "Boggeryk", "Bogham", "Bognash", "Bogodug", "Bogzul", "Bolg", "Bolgrul", "Borab", "Borbuz", "Borgath", "Borgh", "Bormolg", "Borolg", "Borth", "Borz", "Borzighu", "Borzugh", "Braadoth", "Braghul", "Brog", "Brogdul", "Brugagikh", "Brugdush", "Brulak", "Bugnerg", "Bugunh", "Bulg", "Bullig", "Bulugbek", "Bulzog", "Bulozog", "Bumnog", "Buragrub", "Burzgrag", "Burzunguk", "Burzura", "Buzog", "Carzog", "Charlvain", "Cognor", "Dagnub", "Dorzogg", "Dromash", "Dugakh", "Dugan", "Dugroth", "Dugtosh", "Dugug", "Dugugikh", "Dular", "Dulph", "Dulphago", "Dulrat", "Dumolg", "Durak", "Dushgor", "Dushkul", "Dushugg", "Fangoz", "Farbalg", "Fheg", "Gahgdar", "Gahznar", "Gard", "Gargak", "Garmeg", "Garnikh", "Gashdug", "Gasheg", "Gezdak", "Gezorz", "Ghak", "Ghaknag", "Ghamron", "Ghamulg", "Ghashur", "Ghatrugh", "Ghaturn", "Ghaz", "Ghobargh", "Ghogurz", "Ghornag", "Ghornugag", "Ghrategg", "Ghromrash", "Gladba", "Glag", "Glagbor", "Glamalg", "Glaz", "Glazgor", "Glazulg", "Glegokh", "Gloorag", "Gloorot", "Glorgzorgo", "Gloth", "Glothozug", "Glud", "Glundeg", "Glunrum", "Glunurgakh", "Glurdag", "Glurnt", "Glushonkh", "Gluthob", "Gluthush", "Gobur", "Goburbak", "Godrun", "Gogaz", "Gogbag", "Gogrikh", "Goh", "Gohazgu", "Golbag", "Golg", "Goorgul", "Gorak", "Goramalg", "Gorbakh", "Gorblad", "Gorbu", "Gordag", "Gorgath", "Gorgrolg", "Gorlar", "Gorotho", "Gorrath", "Gorzesh", "Gothurg", "Gozarth", "Graalug", "Gralturg", "Grashub", "Gravik", "Grezgor", "Grishduf", "Grodagur", "Grodoguz", "Gronov", "Grookh", "Grubdosh", "Grudogub", "Grugnur", "Grulbash", "Gruldum", "Gruloq", "Gruluk", "Grulzul", "Grumth", "Grunyun", "Grushbub", "Grushnag", "Gruudus", "Gruzdash", "Gruznak", "Gulargh", "Gulburz", "Gulug", "Gulzog", "Gunagud", "Gurg", "Gurgozod", "Gurlak", "Guruzug", "Guzg", "Gwilherm", "Hagard", "Ilthag", "Inazzur", "Kargnuth", "Kazok", "Kelrog", "Kentosh", "Khal", "Khamagash", "Kharsh", "Kharsthun", "Khoruzoth", "Khralek", "Kurog", "Kirgut", "Klang", "Klovag", "Kogaz", "Kradauk", "Krog", "Krogrash", "Kulth", "Kurd", "Kurlash", "Lagarg", "Lagrog", "Lahkgarg", "Lakhalg", "Larhoth", "Larob", "Lashbag", "Latumph", "Laurig", "Lazgel", "Lob", "Logbur", "Logogru", "Logrun", "Lorbash", "Lothdush", "Lothgud", "Lozruth", "Lug", "Lugbagg", "Lugbur", "Lugdakh", "Lugdugul", "Lugnikh", "Lugolg", "Lugrots", "Lugrun", "Lugzod", "Lum", "Lumgol", "Lungruk", "Lunk", "Lurash", "Lurbozog", "Lurg", "Lurgonash", "Luz", "Luzmash", "Maaga", "Mag", "Magunh", "Makhoguz", "Makhug", "Marzul", "Maugash", "Mazabakh", "Mazgro", "Mazogug", "Mekag", "Mog", "Mogazgur", "Mogrub", "Mokhul", "Mordrog", "Mordugul", "Mordularg", "Morg", "Morgaz", "Morgbrath", "Morlak", "Morothmash", "Morotub", "Mort", "Mothozog", "Muduk", "Mudush", "Muglugd", "Mulatub", "Mulgargh", "Mulgu", "Murdodosh", "Murgoz", "Murgrud", "Murkh", "Murlog", "Murzog", "Muzbar", "Muzdrulz", "Muzgalg", "Muzgash", "Muzgu", "Muzogu", "Nagoth", "Nagrul", "Nahzgra", "Nahzush", "Namoroth", "Narazz", "Nargbagorn", "Narhag", "Narkhagikh", "Narkhozikh", "Narkhukulg", "Narkularz", "Nash", "Nenesh", "Norgol", "Nugok", "Nugwugg", "Nunkuk", "Obdeg", "Obgol", "Obgurob", "Obrash", "Ofglog", "Ogmash", "Ogog", "Ogozod", "Ogruk", "Ogularz", "Ogumalg", "Ogzor", "Olfim", "Olfin", "Olgol", "Olugush", "Ontogu", "Oodeg", "Oodegu", "Oorg", "Oorgurn", "Oorlug", "Ordooth", "Orgak", "Orgdugrash", "Orgotash", "Orntosh", "Orzbara", "Orzuk", "Osgrikh", "Osgulug", "Othbug", "Othigu", "Othogor", "Othohoth", "Otholug", "Othukul", "Othulg", "Othzog", "Ozor", "Pergol", "ON:Putor gro-Burku", "Rablarz", "Ragbul", "Ragbur", "Ragnast", "Ramazbur", "Ramorgol", "Ramosh", "Razgor", "Razgugul", "Rhosh", "Rogbum", "Rognar", "Rogrug", "Rogurog", "Rokaug", "Rokut", "Roog", "Rooglag", "Rorburz", "Rozag", "Rugdugbash", "Rugmeg", "Ruzgrol", "Sgolag", "Shab", "Shagrod", "Shakh", "Shakhighu", "Shamar", "Shamlakh", "Sharkagub", "Sharkuzog", "Sharnag", "Shogorn", "Shugral", "Shukul", "Shulthog", "Shurkol", "Skagurn", "Skagwar", "Skalgunh", "Skalguth", "Skarath", "Skordo", "Skulzak", "Slagwug", "Slayag", "Smagbogoth", "Smauk", "Snagbash", "Snagg", "Snagh", "Snakh", "Snakzut", "Snalikh", "Snarbugag", "Snargorg", "Snazumph", "Snegbug", "Snegburgak", "Snegh", "Snikhbat", "Snoog", "Snoorg", "Snugar", "Snugok", "Snukh", "Snushbat", "Sogh", "Spagel", "Storgh", "Stugbrulz", "Szugburg", "Targoth", "Tazgol", "Tazgul", "Thagbush", "Thakh", "Thakush", "Tharag", "Tharkul", "Thaz", "Thazeg", "Thaznog", "Thegur", "Tholog", "Thorzh", "Thorzhul", "Thrag", "Thragdosh", "Thragosh", "Threg", "Thrug", "Thrugb", "Thukbug", "Thungdosh", "Todrak", "Togbrig", "Tograz", "Torg", "Torug", "Tugawuz", "Tumuthag", "Tungthu", "Ufthag", "Ugdush", "Uggnath", "Ugorz", "Ugruntuk", "Ugurz", "Ulagash", "Ulagug", "Ulgdagorn", "Ulghesh", "Undrigug", "Undugar", "Ungruk", "Unrahg", "Unthrikh", "Uragor", "Urak", "Urdbug", "Urgdosh", "Urok", "Ushang", "Usn", "Usnagikh", "Uthik", "Uulgarg", "Uuth", "Uznom", "Vargos", "Waghuth", "Wardush", "Wort", "Yagorkh", "Yagramak", "Yakegg", "Yargob", "Yarnag", "Yat", "Yggnast", "Yggoz", "Yggruk", "Yzzgol", "Zagh", "Zaghurbak", "Zagrakh", "Zagrugh", "Zbulg", "Zegol", "Zgog", "Zhagush", "Zhosh", "Zilbash", "Zogbag", "Zulgozu", "Zungarg", "Zunlog" };
        private List<string> femaleNames = new List<string> { "Abimfash", "Adkul", "Adlugbuk", "Agazu", "Agdesh", "Aglash", "Agli", "Agrash", "Agzurz", "Akash", "Akgruhl", "Akkra", "Aklash", "Alga", "Arakh", "Argulla", "Argurgol", "Arzakh", "Arzorag", "Ashaka", "Ashgara", "Ashgel", "Ashrashag", "Atoga", "Atorag", "Atugol", "Aza", "Azabesh", "Azadhai", "Azhnakha", "Azhnolga", "Azhnura", "Azilkh", "Azlakha", "Azulga", "Baag", "Baagug", "Badush", "Bafthaka", "Bagrugbesh", "Bagul", "Bagula", "Barza", "Barazal", "Batara", "Batasha", "Batorabesh", "Bazbava", "Bazgara", "Bhagruan", "Bluga", "Bolash", "Bolugbeka", "Borbgur", "Borgburakh", "Borgdorga", "Borgrara", "Boroth", "Borzog", "Bugbekh", "Bugbesh", "Bugha", "Bulag", "Bularkh", "Bulfor", "Bumava", "Bumbuk", "Dagarha", "Drienne", "Droka", "Dufbash", "Dulfra", "Dulfraga", "Dulkhi", "Dulroi", "Dumoga", "Dumuguk", "Dumurzog", "Duragma", "Durga", "Durgura", "Durhaz", "Durida", "Durogbesh", "Dushug", "Fnagdesh", "Gahgra", "Gargum", "Garl", "Garlor", "Garlub", "Garotusha", "Ghamzeh", "Gharakul", "Gharn", "Ghat", "Gheshol", "Ghogogg", "Ghorzolga", "Ghratutha", "Glagag", "Glagosh", "Glarikha", "Glash", "Glath", "Glesh", "Glolbikla", "Glothum", "Glurbasha", "Glurduk", "Glurmghal", "Gluth", "Gluthesh", "Gnush", "Gogul", "Golga", "Gondubaga", "Goorga", "Grabash", "Graghesh", "Grahla", "Grahuar", "Graklha", "Grash", "Grashla", "Grashug", "Grazda", "Grazubesha", "Grenbet", "Groddi", "Grubalash", "Grubathag", "Grugleg", "Grumgha", "Grundag", "Gruzbura", "Guazh", "Gula", "Gulara", "Gulgula", "Gulorz", "Gulugash", "Gulza", "Gurhul", "Gursthuk", "Gurum", "Guth", "Guurzash", "Guuth", "Guz", "Guzash", "Guzmara", "Haghai", "Harza", "Hurabesh", "Ilg", "Irsugha", "Jorthan", "Kashurthag", "Khagral", "Khagruk", "Khaguga", "Khaguur", "Khazrakh", "Kora", "Korgha", "Kroma", "Kruaga", "Kuhlon", "Kurz", "Lagabul", "Laganakh", "Lagbaal", "Lagbuga", "Lagra", "Lagruda", "Lahzga", "Lakhazga", "Lamazh", "Lambur", "Lamburak", "Lamugbek", "Lamur", "Larzgug", "Lashakh", "Lashbesh", "Lashdura", "Lashgikh", "Lashgurgol", "Lazghal", "Lazgara", "Legdul", "Mugaga", "Lig", "Logdotha", "Loglorag", "Logru", "Lokra", "Lorak", "Lorogdu", "Luga", "Lugharz", "Luglorash", "Lugrugha", "Lurgush", "Luruzesh", "Lurz", "Mabgrorga", "Mabgrubaga", "Mabgruhl", "Magula", "Maraka", "Marutha", "Maugruhl", "Mazgroth", "Mazrah", "Megruk", "Moglurkgul", "Mograg", "Mogul", "Mordra", "Morga", "Mornamph", "Morndolag", "Mozgosh", "Mugumurn", "Muguur", "Multa", "Mulzah", "Mulzara", "Murotha", "Murzgut", "Muzgraga", "Narzdush", "Nazdura", "Nazhag", "Nazhataga", "Nazubesh", "Nunchak", "Nuza", "Ogzaz", "Oorga", "Oorza", "Oorzuka", "Orbuhl", "Orcolag", "Ordatha", "Orgotha", "Orlozag", "Orlugash", "Orluguk", "Orthuna", "Orutha", "Orzorga", "Oshgana", "Othbekha", "Othgozag", "Othikha", "Othrika", "Ozrog", "Pruzag", "Pruzga", "Ragbarlag", "Ragushna", "Rakhaz", "Ranarsh", "Razbela", "Rogag", "Rogba", "Rogoga", "Rogzesh", "Roku", "Rolfikha", "Rolga", "Rulbagab", "Rulbza", "Rulfala", "Rulfub", "Rulfuna", "Sgala", "Sgrugbesh", "Sgrugha", "Sgrula", "Shabaga", "Shabeg", "Shabeshga", "Shabgrut", "Shabon", "Shagareg", "Shagduka", "Shagora", "Shagrum", "Shagrush", "Shagura", "Shakul", "Shalug", "Sister", "Shamush", "Shara", "Sharbzur", "Shardush", "Shardzozag", "Shargra", "Sharuk", "Sharushnam", "Shaza", "Shebakh", "Sheluka", "Sholg", "Shubesha", "Shufdal", "Shufthakul", "Shuga", "Shugzar", "Shurkul", "Shuzrag", "Sloogolga", "Sluz", "Snagara", "Snarataga", "Snarga", "Snargara", "Snaruga", "Sneehash", "Snilga", "Snoogh", "Snushbesh", "Solgra", "Stilga", "Stroda", "Stuga", "Stughrush", "Theg", "Thegbesh", "Thegshakul", "Thegshalash", "Theshaga", "Theshgoth", "Thishnaku", "Thogra", "Thrugrak", "Thugnekh", "Thulga", "Thushleg", "Tugha", "Ubzigub", "Ufalga", "Ufgabesh", "Ufgaz", "Ufgel", "Ufgra", "Uftheg", "Ugarnesh", "Ugduk", "Ugrash", "Ugrush", "Uldushna", "Ulg", "Ulgush", "Ulsha", "Ulu", "Ulubesh", "Uluga", "Ulukhaz", "Umbugbek", "Umgubesh", "Umutha", "Umzolabesh", "Undorga", "Undusha", "Uratag", "Urbzag", "Urdboga", "Urgarlag", "Uruka", "Urzula", "Usha", "Ushaga", "Ushenat", "Ushruka", "Ushuta", "Uzka", "Vosh", "Vumnish", "Vush", "Yakhu", "Yarlak", "Yarulorz", "Yatular", "Yazara", "Yazgruga", "Yazoga", "Zaag", "Zagla", "Zagula", "Zubesha", "Zugh", "Zuugarz", "Zuuthag", "Zuuthusha" };
        private List<string> lastNames = new List<string> { "Agdur", "Arzug", "Badbul", "Bagol", "Barbol", "Bashnarz", "Bazgar", "Birgo", "Bor", "Borgakh", "Borgub", "Born", "Bugurz", "Bulfimorn", "Bumolg", "Burbulg", "Burgal", "Burku", "Burz", "Buzbee", "Dasik", "Drol", "Drom", "Drublog", "Dugronk", "Dulak", "Durbug", "Durgamph", "Durgoth", "Dushnikh", "Fakal", "Garbug", "Gatuk", "Ghammak", "Ghol", "Ghralog", "Goldfolly", "Gorzoth", "Gum", "Gurba", "Guthra", "Khamagash", "Khambol", "Khamug", "Khargub", "Khazgur", "Korith", "Korlag", "Korma", "Kruts", "Kush", "Ladba", "Logdum", "Lort", "Lumborn", "Luruk", "Madba", "Makla", "Malorz", "Marguz", "Mashul", "Morad", "Morkul", "Murgob", "Murkha", "Murug", "Namor", "Nar", "Narzul", "Nogremor", "Oglurn", "Olub", "Oluk", "Orgak", "Orguk", "Othmog", "gro-Ram", "Rimat", "Ruguk", "Ruumsh", "Shagob", "Shagrak", "Shagronk", "Shatub", "Shatur", "Shazgul", "Shelakh", "Shelob", "Sheluk", "Shub", "Shurkul", "Sgrugdul", "Shugduk", "Shugdurbam", "Shugharz", "Shuhgharz", "Stugbaz", "Stugh", "Thormok", "Ugrush", "Urku", "Urkub", "Urula", "Usharku", "Uzguk", "Vortag", "Wroggin", "Yarzol", "Yggrub" };
        private static Random random = new Random();
        private Gender gender;
        private string fatherLastname;
        private string motherLastname;
        private string stronghold;

        public void SetGender(Gender gender)
        {
            this.gender = gender;
        }

        public void SetFatherLastName(string fatherLastname)
        {
            this.fatherLastname = fatherLastname;
        }

        public void SetMotherLastName(string motherLastname)
        {
            this.motherLastname = motherLastname;
        }

        public void SetStronhold(string stronghold)
        {
            this.stronghold = stronghold;
        }

        public string GetFirstName()
        {
            switch (gender)
            {
                case Gender.Male:
                    return maleNames.ElementAt(random.Next(maleNames.Count - 1));

                case Gender.Female:
                    return femaleNames.ElementAt(random.Next(femaleNames.Count - 1));

                default:
                    throw new IndexOutOfRangeException();
            }
        }

        public string GetLastName()
        {
            if (gender == Gender.Male)
                if (!string.IsNullOrWhiteSpace(fatherLastname))
                    return fatherLastname;
                else if (!string.IsNullOrWhiteSpace(motherLastname))
                    return motherLastname;

            if (gender == Gender.Female)
                if (!string.IsNullOrWhiteSpace(motherLastname))
                    return motherLastname;
                else if (!string.IsNullOrWhiteSpace(fatherLastname))
                    return fatherLastname;

            if (!string.IsNullOrWhiteSpace(stronghold))
                return stronghold;
            else
                return lastNames.ElementAt(random.Next(lastNames.Count - 1));
        }

        public string GetLastNamePrefix()
        {
            switch (gender)
            {
                case Gender.Male:
                    return "gro-";

                case Gender.Female:
                    return "gra-";

                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BridgeDemo.Helpers
{
    public static class ListExtensions
    {
        private static Random random = new Random();

        public static T GetRandomElement<T>(this List<T> collection)
        {
            return collection.ElementAt(random.Next(collection.Count - 1));
        }
    }
}
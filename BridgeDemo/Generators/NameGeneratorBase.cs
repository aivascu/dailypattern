﻿using BridgeDemo.Exporters;

namespace BridgeDemo.Generators
{
    public abstract class NameGeneratorBase : INameGenerator
    {
        protected IExporter exporter;

        protected IFirstNameGenerator firstNameGenerator;
        protected ILastNameGenerator lastNameGenerator;

        public NameGeneratorBase(IFirstNameGenerator firstNameGenerator, ILastNameGenerator lastNameGenerator, IExporter exporter)
        {
            this.firstNameGenerator = firstNameGenerator;
            this.lastNameGenerator = lastNameGenerator;
            this.exporter = exporter;
        }

        public void SetFirstNameGenerator(IFirstNameGenerator firstNameGenerator)
        {
            this.firstNameGenerator = firstNameGenerator;
        }

        public void SetLastNameGenerator(ILastNameGenerator lastNameGenerator)
        {
            this.lastNameGenerator = lastNameGenerator;
        }

        public virtual void Export()
        {
            exporter.Export($"{firstNameGenerator.GetFirstName()} {lastNameGenerator.GetLastName()}");
        }
    }
}
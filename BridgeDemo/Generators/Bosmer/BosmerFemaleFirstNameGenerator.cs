﻿using BridgeDemo.Helpers;
using System.Collections.Generic;

namespace BridgeDemo.Generators.Bosmer
{
    public class BosmerFemaleFirstNameGenerator : IFirstNameGenerator
    {
        private List<string> prefixes = new List<string> { "Cas", "Cyl", "Des", "Lare", "Lego", "Lilis", "Min", "Phy", "Rilli", "Si", "U" };
        private List<string> suffixes = new List<string> { "dra", "fina", "gina", "landra", "lerva", "na", "nia", "sa", "sandra", "thia" };

        public string GetFirstName()
        {
            return $"{prefixes.GetRandomElement()}{suffixes.GetRandomElement()}";
        }
    }
}
﻿using BridgeDemo.Helpers;
using System.Collections.Generic;

namespace BridgeDemo.Generators.Bosmer
{
    public class BosmerLastNameGenerator : ILastNameGenerator
    {
        private List<string> prefixes = new List<string> { "Blue", "Fern", "Forest", "Ivy", "Moss", "Night", "Oak", "Pine", "River", "Shady", "Spring", "Willow" };
        private List<string> suffixes = new List<string> { "brook", "dale", "hollow", "lake", "pool", "run", "shade", "sky", "thorn", "vale", "wind", "wood" };

        public string GetLastName()
        {
            return $"{prefixes.GetRandomElement()}{suffixes.GetRandomElement()}";
        }
    }
}
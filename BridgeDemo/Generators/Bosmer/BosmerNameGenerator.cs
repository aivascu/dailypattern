﻿using BridgeDemo.Exporters;
using BridgeDemo.Generators.Bosmer;

namespace BridgeDemo.Generators
{
    public class BosmerNameGenerator : NameGeneratorBase
    {
        public BosmerNameGenerator(IExporter exporter)
            : base(new BosmerMaleFirstNameGenerator(), new BosmerLastNameGenerator(), exporter)
        {
        }
    }
}
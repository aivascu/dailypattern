﻿using BridgeDemo.Helpers;
using System.Collections.Generic;

namespace BridgeDemo.Generators.Bosmer
{
    public class BosmerMaleFirstNameGenerator : IFirstNameGenerator
    {
        private List<string> prefixes = new List<string> { "An", "Ara", "Ar", "Co", "Elis", "Karo", "Lego", "Li", "Pali", "Ria", "Sil", "Ta" };
        private List<string> suffixes = new List<string> { "cher", "dell", "driel", "gan", "gorn", "las", "man", "nis", "nor", "rim", "tan", "van" };

        public string GetFirstName()
        {
            return $"{prefixes.GetRandomElement()}{suffixes.GetRandomElement()}";
        }
    }
}
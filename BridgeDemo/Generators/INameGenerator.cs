﻿namespace BridgeDemo
{
    public interface INameGenerator
    {
        void SetFirstNameGenerator(IFirstNameGenerator firstNameGenerator);

        void SetLastNameGenerator(ILastNameGenerator lastNameGenerator);

        void Export();
    }

    public interface IFirstNameGenerator
    {
        string GetFirstName();
    }

    public interface ILastNameGenerator
    {
        string GetLastName();
    }
}
﻿using BridgeDemo.Helpers;
using System.Collections.Generic;

namespace BridgeDemo.Generators.Orc
{
    public abstract class OrcLastNameGenerator : ILastNameGenerator
    {
        private List<string> lastNames = new List<string> { "Agdur", "Arzug", "Badbul", "Bagol", "Barbol", "Bashnarz", "Bazgar", "Birgo", "Bor", "Borgakh", "Borgub", "Born", "Bugurz", "Bulfimorn", "Bumolg", "Burbulg", "Burgal", "Burku", "Burz", "Buzbee", "Dasik", "Drol", "Drom", "Drublog", "Dugronk", "Dulak", "Durbug", "Durgamph", "Durgoth", "Dushnikh", "Fakal", "Garbug", "Gatuk", "Ghammak", "Ghol", "Ghralog", "Goldfolly", "Gorzoth", "Gum", "Gurba", "Guthra", "Khamagash", "Khambol", "Khamug", "Khargub", "Khazgur", "Korith", "Korlag", "Korma", "Kruts", "Kush", "Ladba", "Logdum", "Lort", "Lumborn", "Luruk", "Madba", "Makla", "Malorz", "Marguz", "Mashul", "Morad", "Morkul", "Murgob", "Murkha", "Murug", "Namor", "Nar", "Narzul", "Nogremor", "Oglurn", "Olub", "Oluk", "Orgak", "Orguk", "Othmog", "gro-Ram", "Rimat", "Ruguk", "Ruumsh", "Shagob", "Shagrak", "Shagronk", "Shatub", "Shatur", "Shazgul", "Shelakh", "Shelob", "Sheluk", "Shub", "Shurkul", "Sgrugdul", "Shugduk", "Shugdurbam", "Shugharz", "Shuhgharz", "Stugbaz", "Stugh", "Thormok", "Ugrush", "Urku", "Urkub", "Urula", "Usharku", "Uzguk", "Vortag", "Wroggin", "Yarzol", "Yggrub" };

        public virtual string GetLastName()
        {
            return lastNames.GetRandomElement();
        }
    }
}
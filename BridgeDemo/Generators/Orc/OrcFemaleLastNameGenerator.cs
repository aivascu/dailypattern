﻿namespace BridgeDemo.Generators.Orc
{
    public class OrcFemaleLastNameGenerator : OrcLastNameGenerator
    {
        public override string GetLastName()
        {
            return $"gra-{base.GetLastName()}";
        }
    }
}
﻿using BridgeDemo.Exporters;
using BridgeDemo.Generators.Orc;

namespace BridgeDemo.Generators
{
    public class OrcNameGenerator : NameGeneratorBase
    {
        public OrcNameGenerator(IExporter exporter)
            : base(new OrcMaleFirstNameGenerator(), new OrcMaleLastNameGenerator(), exporter)
        {
        }
    }
}
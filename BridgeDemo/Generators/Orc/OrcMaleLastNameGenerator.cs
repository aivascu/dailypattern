﻿namespace BridgeDemo.Generators.Orc
{
    public class OrcMaleLastNameGenerator : OrcLastNameGenerator
    {
        public override string GetLastName()
        {
            return $"gro-{base.GetLastName()}";
        }
    }
}
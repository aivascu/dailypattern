﻿using System;

namespace BridgeDemo.Exporters
{
    public class ConsoleExporter : ExporterBase
    {
        public override void Export(string data)
        {
            Console.WriteLine(data);
            Console.WriteLine("INFO: Name {0} exported to console!", data);
        }
    }
}
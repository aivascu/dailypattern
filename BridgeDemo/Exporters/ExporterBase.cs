﻿namespace BridgeDemo.Exporters
{
    public abstract class ExporterBase : IExporter
    {
        public abstract void Export(string data);
    }
}
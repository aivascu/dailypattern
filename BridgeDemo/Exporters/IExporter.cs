﻿namespace BridgeDemo.Exporters
{
    public interface IExporter
    {
        void Export(string data);
    }
}
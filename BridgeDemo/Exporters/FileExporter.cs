﻿using System.IO;

namespace BridgeDemo.Exporters
{
    public class FileExporter : ExporterBase
    {
        private string fileName = @"D:\names.txt";

        public override void Export(string data)
        {
            if (!File.Exists(fileName))
            {
                using (var writer = File.CreateText(fileName))
                {
                    writer.WriteLine(data);
                }
            }
            else
            {
                using (var writer = File.AppendText(fileName))
                {
                    writer.WriteLine(data);
                }
            }

            //Console.WriteLine("INFO: Name {0} exported to file!", data);
        }
    }
}
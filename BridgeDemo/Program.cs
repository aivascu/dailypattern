﻿using BridgeDemo.Exporters;
using BridgeDemo.Generators;

namespace BridgeDemo
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            IExporter consoleExporter = new ConsoleExporter();
            IExporter fileExporter = new FileExporter();

            NameGeneratorBase orcNameGenerator = new OrcNameGenerator(consoleExporter);
            NameGeneratorBase bosmerNameGenerator = new BosmerNameGenerator(fileExporter);

            for (int i = 0; i < 100; i++)
            {
                bosmerNameGenerator.Export();
                orcNameGenerator.Export();
            }
        }
    }
}
﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using M3U.NET.Extensions;

#endregion

namespace M3U.NET
{
    public class M3UFile : ICollection<M3UEntry>
    {
        private readonly List<M3UEntry> _entries = new List<M3UEntry>();

        public M3UEntry this[int index]
        {
            get { return _entries[index]; }
        }

        public void Load(string fileName, bool resolveRelativePaths = false)
        {
            _entries.Clear();

            using (var reader = new StreamReader(fileName))
            {
                string line;
                var lineCount = 0;

                M3UEntry entry = null;

                while ((line = reader.ReadLine()) != null)
                {
                    if (lineCount == 0)
                        if (string.CompareOrdinal(line, "#EXTM3U") != 0)
                            throw new M3UException("M3U header is missing.");
                        else
                        {
                            lineCount++;
                            continue;
                        }

                    if (line.StartsWith("#"))
                    {
                        var regex = new Regex(@"(?:\#EXTINF\:)(?<time>\d*|-1),(?<title>.*)$");
                        var match = regex.Match(line);

                        if (!match.Success)
                        {
                            lineCount++;
                            continue;
                        }

                        var title = match.Groups["title"].Value;
                        var time = match.Groups["time"].Value;

                        int seconds;
                        if (!int.TryParse(time, out seconds))
                            throw new M3UException("Invalid track duration.");

                        var duration = TimeSpan.FromSeconds(seconds);

                        entry = new M3UEntry
                        {
                            FullTitle = title,
                            Length = duration,
                            PathUri = null
                        };

                        lineCount++;
                        continue;
                    }

                    Uri pathUri;
                    if (!Uri.TryCreate(line, UriKind.RelativeOrAbsolute, out pathUri))
                        throw new M3UException("Invalid entry path.");

                    if (entry != null)
                    {
                        entry.PathUri = pathUri;

                        _entries.Add(entry);
                    }

                    entry = null;

                    lineCount++;
                }
            }
        }

        public void Save(string fileName, bool useAbsolutePaths = false, bool useLocalFilePath = true)
        {
            var workingUri = new Uri(Path.GetDirectoryName(fileName));

            using (var writer = new StreamWriter(fileName))
            {
                writer.WriteLine("#EXTM3U");

                foreach (var entry in this)
                {
                    writer.WriteLine("#EXTINF:{0},{1}", entry.Length.TotalSeconds, entry.FullTitle);

                    if (entry.PathUri.IsFile && useLocalFilePath)
                        writer.WriteLine(Uri.UnescapeDataString(entry.PathUri.LocalPath));
                    else if (!entry.PathUri.IsAbsoluteUri && useAbsolutePaths)
                        writer.WriteLine(entry.PathUri.MakeAbsoluteUri(workingUri));
                    else
                        writer.WriteLine(entry.PathUri);
                }
            }
        }

        #region Implementation of IEnumerable

        public IEnumerator<M3UEntry> GetEnumerator()
        {
            return ((IEnumerable<M3UEntry>)_entries).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of ICollection<M3UEntry>

        public void Add(M3UEntry item)
        {
            _entries.Add(item);
        }

        public void Clear()
        {
            _entries.Clear();
        }

        public bool Contains(M3UEntry item)
        {
            return _entries.Contains(item);
        }

        public void CopyTo(M3UEntry[] array, int arrayIndex)
        {
            _entries.CopyTo(array, arrayIndex);
        }

        public bool Remove(M3UEntry item)
        {
            return _entries.Remove(item);
        }

        public int Count
        {
            get { return _entries.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        public M3UEntry Find(Predicate<M3UEntry> match)
        {
            return _entries.Find(match);
        }

        public List<M3UEntry> FindAll(Predicate<M3UEntry> match)
        {
            return _entries.FindAll(match);
        }
    }
}
﻿using System;

namespace M3U.NET
{
    public class M3UEntry
    {
        public TimeSpan Length { get; set; }
        public string FullTitle { get; set; }
        public Uri PathUri { get; set; }
    }
}
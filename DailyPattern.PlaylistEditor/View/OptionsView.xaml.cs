﻿using System.Windows.Controls;

namespace DailyPattern.PlaylistEditor.View
{
    /// <summary>
    /// Description for OptionsView.
    /// </summary>
    public partial class OptionsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the OptionsView class.
        /// </summary>
        public OptionsView()
        {
            InitializeComponent();
        }
    }
}
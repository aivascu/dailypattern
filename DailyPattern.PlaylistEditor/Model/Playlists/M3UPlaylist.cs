﻿using System.Collections.Generic;
using System.Linq;
using DailyPattern.PlaylistEditor.Model.Adapters;
using DailyPattern.PlaylistEditor.Resource;
using DailyPattern.PlaylistEditor.Service.Interfaces;
using M3U.NET;

namespace DailyPattern.PlaylistEditor.Model.Playlists
{
    public class M3UPlaylist : IPlaylist
    {
        private M3UFile playlistFile;
        private IList<IPlaylistItem> items;
        private string playlistPath;

        public M3UPlaylist()
        {
            items = new List<IPlaylistItem>();
            playlistFile = new M3UFile();
        }

        public M3UPlaylist(IPlaylist playlist)
        {
            items = playlist.Items;
            playlistFile = new M3UFile();
            UpdateEntries();
        }

        public IList<IPlaylistItem> Items => items;

        public string Path => playlistPath;

        public PlaylistType Type => PlaylistType.M3U;

        public void Open(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return;

            playlistFile.Load(path, true);
            playlistPath = path;

            Items.Clear();

            items = playlistFile.Select(f => new M3UItemAdapter(f) as IPlaylistItem).ToList();
        }

        public void Save(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return;

            playlistFile.Save(path, true);
        }

        public void AddTrack(IPlaylistItem item)
        {
            Items.Add(item);
            UpdateEntries();
        }

        public void RemoveTrack(int index)
        {
            Items.RemoveAt(index);
            UpdateEntries();
        }

        public void MoveTrack(int oldIndex, int newIndex)
        {
            var playlistItem = Items.ElementAtOrDefault(oldIndex);

            if (playlistItem == null)
                return;

            Items.RemoveAt(oldIndex);
            Items.Insert(newIndex, playlistItem);
            UpdateEntries();
        }

        private void UpdateEntries()
        {
            playlistFile.Clear();
            foreach (var item in Items)
            {
                playlistFile.Add(new M3UItemAdapter(item));
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using DailyPattern.PlaylistEditor.Model.Adapters;
using DailyPattern.PlaylistEditor.Resource;
using DailyPattern.PlaylistEditor.Service.Interfaces;
using HorrorSoft.PLS;

namespace DailyPattern.PlaylistEditor.Model.Playlists
{
    public class WinampPlaylist : IPlaylist
    {
        private PLSFile playlistFile;
        private IList<IPlaylistItem> items;
        private string playlistPath;

        public WinampPlaylist()
        {
            items = new List<IPlaylistItem>();
            playlistFile = new PLSFile();
        }

        public WinampPlaylist(IPlaylist playlist)
        {
            playlistFile = new PLSFile();
            this.items = playlist.Items;
            UpdateEntries();
        }

        public IList<IPlaylistItem> Items => items;
        public PlaylistType Type => PlaylistType.Winamp;
        public string Path => playlistPath;

        public void Open(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return;

            playlistFile.Load(path, true);
            playlistPath = path;

            Items.Clear();
            items = playlistFile.Select(f => new PlsItemAdapter(f) as IPlaylistItem).ToList();
        }

        public void Save(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return;

            playlistFile.Save(path, true);
        }

        public void AddTrack(IPlaylistItem item)
        {
            Items.Add(item);
            UpdateEntries();
        }

        public void RemoveTrack(int index)
        {
            Items.RemoveAt(index);
            UpdateEntries();
        }

        public void MoveTrack(int oldIndex, int newIndex)
        {
            var playlistItem = Items.ElementAtOrDefault(oldIndex);

            if (playlistItem == null)
                return;

            Items.RemoveAt(oldIndex);
            Items.Insert(newIndex, playlistItem);
            UpdateEntries();
        }

        private void UpdateEntries()
        {
            playlistFile.Clear();
            foreach (var item in Items)
            {
                playlistFile.Add(new PlsItemAdapter(item));
            }
        }
    }
}
﻿using System;
using System.Text.RegularExpressions;
using HorrorSoft.PLS;

namespace DailyPattern.PlaylistEditor.Model.Adapters
{
    public class PlsItemAdapter : PLSEntry, IPlaylistItem
    {
        private string artist;
        private string title;

        public PlsItemAdapter()
        {
        }

        public PlsItemAdapter(PLSEntry entry)
        {
            FullTitle = entry.FullTitle;
            Length = entry.Length;
            FilePath = entry.FilePath;
            ParseTitle(FullTitle, out artist, out title);
        }

        public PlsItemAdapter(IPlaylistItem item)
        {
            Artist = item.Artist;
            Title = item.Title;
            Length = item.Duration;
            FilePath = new Uri(item.Path);
        }

        public string Artist
        {
            get { return artist; }
            set
            {
                artist = value;
                FullTitle = string.IsNullOrWhiteSpace(Artist) ? Title : $"{Artist} - {Title}";
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                FullTitle = string.IsNullOrWhiteSpace(Artist) ? Title : $"{Artist} - {Title}";
            }
        }

        public TimeSpan Duration
        {
            get { return Length; }
            set { Length = value; }
        }

        public string Path
        {
            get { return FilePath.LocalPath; }
            set { FilePath = new Uri(value); }
        }

        private static void ParseTitle(string input, out string artist, out string title)
        {
            var regex = new Regex(@"^(?<artist>.*)\s-\s(?<title>.*)$");
            var match = regex.Match(input);

            if (match.Success)
            {
                artist = match.Groups["artist"].Value;
                title = match.Groups["title"].Value;
                return;
            }

            artist = string.Empty;
            title = string.Empty;
        }
    }
}
using System;

namespace DailyPattern.PlaylistEditor.Model
{
    public class PlaylistItem : IPlaylistItem
    {
        private string artist;
        private string title;
        private string path;
        private TimeSpan duration;

        public string Artist
        {
            get { return artist; }
            set
            {
                artist = value;
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
            }
        }

        public string FullTitle => $"{Artist} - {Title}";

        public string Path
        {
            get { return path; }
            set
            {
                path = value;
            }
        }

        public TimeSpan Duration
        {
            get { return duration; }
            set
            {
                duration = value;
            }
        }
    }
}
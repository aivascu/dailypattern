﻿using System;

namespace DailyPattern.PlaylistEditor.Model
{
    public interface IPlaylistItem
    {
        string Artist { get; set; }
        string Title { get; set; }
        TimeSpan Duration { get; set; }
        string Path { get; set; }
    }
}
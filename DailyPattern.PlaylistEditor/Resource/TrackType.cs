﻿namespace DailyPattern.PlaylistEditor.Resource
{
    public enum TrackType
    {
        Mp3 = 1,
        Wma = 2
    }
}
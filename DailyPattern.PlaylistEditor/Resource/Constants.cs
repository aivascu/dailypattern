﻿using System.Collections.Generic;

namespace DailyPattern.PlaylistEditor.Resource
{
    public static class Constants
    {
        public static readonly string Unknown = "Unknown";
        public static readonly string LastUsedPlaylist = "LastUsedPlaylist";

        public static readonly IDictionary<PlaylistType, string> PlaylistFormats = new Dictionary<PlaylistType, string>
        {
            { PlaylistType.Winamp, "Winamp Playlist (*.pls)|*.pls" },
            { PlaylistType.M3U, "M3U (*.m3u)|*.m3u" }
        };

        public static readonly IDictionary<TrackType, string> TrackFormats = new Dictionary<TrackType, string>
        {
            { TrackType.Mp3, "MP3 (*.mp3)|*.mp3" },
            { TrackType.Wma, "Windows Media Audio (*.wma)|*.wma" }
        };

        public static string GetPlaylistFormats()
        {
            return string.Join("|", PlaylistFormats.Values);
        }

        public static string GetTrackFormats()
        {
            return string.Join("|", TrackFormats.Values);
        }
    }
}
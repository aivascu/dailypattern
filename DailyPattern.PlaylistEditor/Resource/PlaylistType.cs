﻿namespace DailyPattern.PlaylistEditor.Resource
{
    public enum PlaylistType
    {
        Unknown = 0,
        Winamp = 1,
        M3U = 2
    }
}
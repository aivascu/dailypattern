﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace DailyPattern.PlaylistEditor.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private ViewModelBase currentViewModel;

        public ViewModelBase CurrentViewModel
        {
            get { return currentViewModel; }
            set
            {
                if (currentViewModel != null && currentViewModel == value)
                    return;
                currentViewModel = value;
                RaisePropertyChanged(() => CurrentViewModel);
            }
        }

        public ICommand ShowPlaylistViewCommand { get; private set; }

        public ICommand ShowOptionsViewCommand { get; private set; }

        public MainViewModel()
        {
            ShowPlaylistViewCommand = new RelayCommand(ShowPlaylistView);
            ShowOptionsViewCommand = new RelayCommand(ShowOptionsView);

            ShowPlaylistView();
        }

        private void ShowOptionsView()
        {
            CurrentViewModel = ViewModelLocator.OptionsViewModel;
        }

        private void ShowPlaylistView()
        {
            CurrentViewModel = ViewModelLocator.PlaylistViewModel;
        }
    }
}
﻿using DailyPattern.PlaylistEditor.Model;
using GalaSoft.MvvmLight;

namespace DailyPattern.PlaylistEditor.ViewModel
{
    public class PlaylistItemViewModel : ObservableObject
    {
        private IPlaylistItem item;

        public PlaylistItemViewModel(IPlaylistItem item)
        {
            this.item = item;
        }

        public string Artist
        {
            get { return item.Artist; }
            set
            {
                item.Artist = value;
                RaisePropertyChanged(() => Artist);
                RaisePropertyChanged(() => FullTitle);
            }
        }

        public string Title
        {
            get { return item.Title; }
            set
            {
                item.Title = value;
                RaisePropertyChanged(() => Title);
                RaisePropertyChanged(() => FullTitle);
            }
        }

        public string FullTitle => string.IsNullOrWhiteSpace(item.Artist) ? Title : $"{Artist} - {Title}";

        public string Length => item.Duration.ToString("g");
    }
}
﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DailyPattern.PlaylistEditor.Resource;
using DailyPattern.PlaylistEditor.Service.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace DailyPattern.PlaylistEditor.ViewModel
{
    public class PlaylistViewModel : ViewModelBase
    {
        private ObservableCollection<PlaylistItemViewModel> playlist;
        private PlaylistItemViewModel selectedItem;
        private readonly IDialogService dialogService;
        private readonly IConfigurationService configurationService;
        private readonly IPlaylistService playlistService;

        public PlaylistViewModel(IDialogService dialogService, IConfigurationService configurationService, IPlaylistService playlistService)
        {
            this.dialogService = dialogService;
            this.configurationService = configurationService;
            this.playlistService = playlistService;

            AddTrackCommand = new RelayCommand(AddTrack);
            ExportPlaylistCommand = new RelayCommand(ExportPlaylist);
            MoveTrackDownCommand = new RelayCommand(MoveTrackDown);
            MoveTrackUpCommand = new RelayCommand(MoveTrackUp);
            UpdateTagsCommand = new RelayCommand(UpdateTags);
            OpenPlaylistCommand = new RelayCommand(OpenPlaylist);
            RemoveTrackCommand = new RelayCommand(RemoveTrack);

            if (playlist == null && configurationService.HasConfig(Constants.LastUsedPlaylist))
            {
                playlistService.Load(configurationService.GetConfiguration(Constants.LastUsedPlaylist));

                playlist = new ObservableCollection<PlaylistItemViewModel>(playlistService.Playlist.Select(i => new PlaylistItemViewModel(i)));
            }
            else
            {
                playlist = new ObservableCollection<PlaylistItemViewModel>();
            }
        }

        public ICommand AddTrackCommand { get; private set; }
        public ICommand ExportPlaylistCommand { get; private set; }
        public ICommand MoveTrackDownCommand { get; private set; }
        public ICommand MoveTrackUpCommand { get; private set; }
        public ICommand UpdateTagsCommand { get; private set; }
        public ICommand OpenPlaylistCommand { get; private set; }
        public ICommand RemoveTrackCommand { get; private set; }

        public PlaylistItemViewModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        public ObservableCollection<PlaylistItemViewModel> Playlist
        {
            get { return playlist; }
            set
            {
                playlist = value;
                RaisePropertyChanged(() => Playlist);
            }
        }

        private void ExportPlaylist()
        {
            var task = dialogService.ShowSaveDialog(Constants.GetPlaylistFormats(), string.Empty, (filePath, filterIndex) =>
            {
                switch ((PlaylistType)filterIndex)
                {
                    case PlaylistType.Winamp:
                        {
                            playlistService.SaveAs(filePath, PlaylistType.Winamp);
                        }
                        break;

                    case PlaylistType.M3U:
                        {
                            playlistService.SaveAs(filePath, PlaylistType.M3U);
                        }
                        break;
                }
            });

            task.RunSynchronously();
            RaisePropertyChanged(() => Playlist);
        }

        private void MoveTrackUp()
        {
            if (SelectedItem == null)
                return;

            var oldIndex = Playlist.IndexOf(SelectedItem);
            playlistService.MoveUp(oldIndex);
            Playlist = new ObservableCollection<PlaylistItemViewModel>(playlistService.Playlist.Select(i => new PlaylistItemViewModel(i)));
            RaisePropertyChanged(() => Playlist);
        }

        private void MoveTrackDown()
        {
            if (SelectedItem == null)
                return;

            var oldIndex = Playlist.IndexOf(SelectedItem);
            playlistService.MoveDown(oldIndex);
            Playlist = new ObservableCollection<PlaylistItemViewModel>(playlistService.Playlist.Select(i => new PlaylistItemViewModel(i)));
            RaisePropertyChanged(() => Playlist);
        }

        private void AddTrack()
        {
            var task = dialogService.ShowOpenDialog(true, Constants.GetTrackFormats(), string.Empty, (filePaths, filterIndex) =>
            {
                if (filePaths == null || !filePaths.Any())
                    return;

                var trackType = (TrackType)filterIndex;

                foreach (var file in filePaths)
                {
                    playlistService.Add(file);
                }
            });

            task.RunSynchronously();
            Playlist = new ObservableCollection<PlaylistItemViewModel>(playlistService.Playlist.Select(i => new PlaylistItemViewModel(i)));
            RaisePropertyChanged(() => Playlist);
        }

        private void RemoveTrack()
        {
            var index = Playlist.IndexOf(SelectedItem);
            playlistService.Remove(index);
            Playlist = new ObservableCollection<PlaylistItemViewModel>(playlistService.Playlist.Select(i => new PlaylistItemViewModel(i)));
            RaisePropertyChanged(() => Playlist);
        }

        private void OpenPlaylist()
        {
            var task = dialogService.ShowOpenDialog(false, Constants.GetPlaylistFormats(), string.Empty, (filePaths, filterIndex) =>
            {
                if (filePaths == null || !filePaths.Any())
                    return;

                var path = filePaths.SingleOrDefault();

                if (!string.IsNullOrWhiteSpace(path))
                {
                    playlistService.Load(path);
                }
                Playlist = new ObservableCollection<PlaylistItemViewModel>(playlistService.Playlist.Select(i => new PlaylistItemViewModel(i)));
                RaisePropertyChanged(() => Playlist);
            });
            task.RunSynchronously();
        }

        private void UpdateTags()
        {
        }
    }
}
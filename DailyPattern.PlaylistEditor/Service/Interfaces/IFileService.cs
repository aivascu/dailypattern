﻿using System;
using DailyPattern.PlaylistEditor.Model;

namespace DailyPattern.PlaylistEditor.Service.Interfaces
{
    public interface IFileService
    {
        IPlaylistItem GetAudioFileInfo(string path);

        IPlaylistItem GetAudioFileInfo(Uri uri);

        bool IsValidPath(string path);

        string GetFileExtension(string path);
    }
}
﻿using System.Collections.Generic;
using DailyPattern.PlaylistEditor.Model;
using DailyPattern.PlaylistEditor.Resource;

namespace DailyPattern.PlaylistEditor.Service.Interfaces
{
    public interface IPlaylistService
    {
        bool Load(string path);

        IEnumerable<IPlaylistItem> Playlist { get; }

        bool Save();

        bool SaveAs(string path, PlaylistType type);

        /// <summary>
        /// Add tracks to the playlist
        /// </summary>
        /// <param name="paths">Paths to the added audio tracks</param>
        void Add(params string[] paths);

        /// <summary>
        /// Removes tracks from the playlist
        /// </summary>
        /// <param name="indexes">Indexes of the tracks to</param>
        void Remove(params int[] indexes);

        void MoveUp(int index);

        void MoveDown(int index);

        void MoveTo(int oldIndex, int newIndex);
    }
}
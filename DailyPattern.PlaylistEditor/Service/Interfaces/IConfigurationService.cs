﻿namespace DailyPattern.PlaylistEditor.Service.Interfaces
{
    public interface IConfigurationService
    {
        string GetConfiguration(string key);

        void SetConfiguration(string key, string value);

        bool HasConfig(string key);
    }
}
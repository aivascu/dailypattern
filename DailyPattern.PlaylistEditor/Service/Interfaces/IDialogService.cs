﻿using System;
using System.Threading.Tasks;

namespace DailyPattern.PlaylistEditor.Service.Interfaces
{
    public interface IDialogService : GalaSoft.MvvmLight.Views.IDialogService
    {
        Task ShowOpenDialog(bool multiselect, string filters, string initialDirectory, Action<string[], int> afterHideCallback);

        Task ShowSaveDialog(string filters, string initialDirectory, Action<string, int> afterHideCallback);
    }
}
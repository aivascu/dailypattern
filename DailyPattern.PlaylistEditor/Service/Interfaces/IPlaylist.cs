﻿using System.Collections.Generic;
using DailyPattern.PlaylistEditor.Model;
using DailyPattern.PlaylistEditor.Resource;

namespace DailyPattern.PlaylistEditor.Service.Interfaces
{
    public interface IPlaylist
    {
        IList<IPlaylistItem> Items { get; }
        PlaylistType Type { get; }
        string Path { get; }

        void Open(string path);

        void Save(string path);

        void AddTrack(IPlaylistItem item);

        void RemoveTrack(int index);

        void MoveTrack(int oldIndex, int newIndex);
    }
}
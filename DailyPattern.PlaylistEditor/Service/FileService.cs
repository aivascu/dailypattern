﻿using System;
using System.IO;
using DailyPattern.PlaylistEditor.Model;
using DailyPattern.PlaylistEditor.Service.Interfaces;
using Id3;

namespace DailyPattern.PlaylistEditor.Service
{
    public class FileService : IFileService
    {
        public IPlaylistItem GetAudioFileInfo(string path)
        {
            IPlaylistItem fileInfo = new PlaylistItem();

            using (var reader = new StreamReader(path))
            using (var mp3file = new Mp3Stream(reader.BaseStream))
            {
                fileInfo.Duration = mp3file.Audio.Duration;
                fileInfo.Title = mp3file.GetTag(Id3TagFamily.FileStartTag).Title;
                fileInfo.Artist = mp3file.GetTag(Id3TagFamily.FileStartTag).Artists;
                fileInfo.Path = path;
            }

            return fileInfo;
        }

        public IPlaylistItem GetAudioFileInfo(Uri uri)
        {
            return GetAudioFileInfo(uri.AbsolutePath);
        }

        public bool IsValidPath(string path)
        {
            var fullPath = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(path))
                    fullPath = Path.GetFullPath(path);
            }
            catch (Exception)
            {
                return false;
            }

            return !string.IsNullOrWhiteSpace(fullPath);
        }

        public string GetFileExtension(string path)
        {
            return Path.GetExtension(path);
        }
    }
}
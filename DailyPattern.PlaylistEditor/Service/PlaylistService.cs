﻿using System;
using System.Collections.Generic;
using System.Linq;
using DailyPattern.PlaylistEditor.Model;
using DailyPattern.PlaylistEditor.Model.Playlists;
using DailyPattern.PlaylistEditor.Resource;
using DailyPattern.PlaylistEditor.Service.Interfaces;

namespace DailyPattern.PlaylistEditor.Service
{
    public class PlaylistService : IPlaylistService
    {
        private readonly IFileService fileService;
        private IPlaylist playlist;

        public PlaylistService(IFileService fileService)
        {
            this.fileService = fileService;
        }

        public bool Load(string path)
        {
            var type = PlaylistType.Unknown;
            if (fileService.IsValidPath(path))
            {
                type = DetectPlaylistType(path);
            }

            switch (type)
            {
                case PlaylistType.Winamp:
                    {
                        playlist = new WinampPlaylist();
                        playlist.Open(path);
                        return true;
                    }

                case PlaylistType.M3U:
                    {
                        playlist = new M3UPlaylist();
                        playlist.Open(path);
                        return true;
                    }

                case PlaylistType.Unknown:
                default:
                    {
                        return false;
                    }
            }
        }

        public IEnumerable<IPlaylistItem> Playlist => playlist.Items;

        public bool Save()
        {
            try
            {
                playlist.Save(playlist.Path);
            }
            catch (Exception)
            {
                // todo log this
                return false;
            }
            return true;
        }

        public bool SaveAs(string newPath, PlaylistType type)
        {
            switch (type)
            {
                case PlaylistType.Winamp:
                    {
                        var newPlaylist = new WinampPlaylist(playlist);
                        newPlaylist.Save(newPath);
                        playlist = newPlaylist;
                        return true;
                    }

                case PlaylistType.M3U:
                    {
                        var newPlaylist = new M3UPlaylist(playlist);
                        newPlaylist.Save(newPath);
                        playlist = newPlaylist;
                        return true;
                    }

                default:
                    {
                        return false;
                    }
            }
        }

        public void Add(params string[] paths)
        {
            foreach (var fileInfo in paths.Select(path => fileService.GetAudioFileInfo(path)))
            {
                playlist.AddTrack(fileInfo);
            }
        }

        public void Remove(params int[] indexes)
        {
            foreach (var index in indexes)
            {
                playlist.RemoveTrack(index);
            }
        }

        public void MoveUp(int index)
        {
            playlist.MoveTrack(index, --index);
        }

        public void MoveDown(int index)
        {
            playlist.MoveTrack(index, ++index);
        }

        public void MoveTo(int oldIndex, int newIndex)
        {
            playlist.MoveTrack(oldIndex, newIndex);
        }

        private PlaylistType DetectPlaylistType(string path)
        {
            var extension = fileService.GetFileExtension(path);
            switch (extension.ToUpperInvariant())
            {
                case ".PLS":
                    return PlaylistType.Winamp;

                case ".M3U":
                    return PlaylistType.M3U;

                default:
                    return PlaylistType.Unknown;
            }
        }
    }
}
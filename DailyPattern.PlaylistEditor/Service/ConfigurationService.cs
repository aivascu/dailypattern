﻿using System.Configuration;
using DailyPattern.PlaylistEditor.Service.Interfaces;

namespace DailyPattern.PlaylistEditor.Service
{
    public class ConfigurationService : IConfigurationService
    {
        public string GetConfiguration(string key)
        {
            return ConfigurationManager.AppSettings.Get(key);
        }

        public void SetConfiguration(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                    settings.Add(key, value);
                else
                    settings[key].Value = value;

                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                //todo log this
            }
        }

        public bool HasConfig(string key)
        {
            return !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get(key));
        }
    }
}
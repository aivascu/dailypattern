﻿using System;
using System.Threading.Tasks;
using System.Windows;
using DailyPattern.PlaylistEditor.Service.Interfaces;
using Microsoft.Win32;

namespace DailyPattern.PlaylistEditor.Service
{
    public class DialogService : IDialogService
    {
        public Task ShowError(string message, string title, string buttonText, Action afterHideCallback)
        {
            var task = new Task(() =>
            {
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                afterHideCallback?.Invoke();
            });
            return task;
        }

        public Task ShowError(Exception error, string title, string buttonText, Action afterHideCallback)
        {
            return ShowError(error.Message, title, buttonText, afterHideCallback);
        }

        public Task ShowMessage(string message, string title)
        {
            var task = new Task(() =>
            {
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            });
            return task;
        }

        public Task ShowMessage(string message, string title, string buttonConfirmText, string buttonCancelText, Action afterHideCallback)
        {
            throw new NotImplementedException();
        }

        public Task ShowMessage(string message, string title, string buttonText, Action afterHideCallback)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ShowMessage(string message, string title, string buttonConfirmText, string buttonCancelText, Action<bool> afterHideCallback)
        {
            throw new NotImplementedException();
        }

        public Task ShowMessageBox(string message, string title)
        {
            throw new NotImplementedException();
        }

        public Task ShowOpenDialog(bool multiselect, string filters, string initialDirectory, Action<string[], int> afterHideCallback)
        {
            initialDirectory = !string.IsNullOrWhiteSpace(initialDirectory)
                ? initialDirectory
                : Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);

            var task = new Task(() =>
            {
                var openFileDialog = new OpenFileDialog
                {
                    Multiselect = multiselect,
                    Filter = filters,
                    InitialDirectory = initialDirectory
                };
                var paths = openFileDialog.ShowDialog() == true ? openFileDialog.FileNames : null;
                var filterIndex = openFileDialog.FilterIndex;
                afterHideCallback?.Invoke(paths, filterIndex);
            });
            return task;
        }

        public Task ShowSaveDialog(string filters, string initialDirectory, Action<string, int> afterHideCallback)
        {
            initialDirectory = !string.IsNullOrWhiteSpace(initialDirectory)
                ? initialDirectory
                : Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);

            var task = new Task(() =>
            {
                var saveFileDialog = new SaveFileDialog
                {
                    Filter = filters,
                    InitialDirectory = initialDirectory
                };

                var path = saveFileDialog.ShowDialog() == true ? saveFileDialog.FileName : null;
                var filterIndex = saveFileDialog.FilterIndex;
                afterHideCallback?.Invoke(path, filterIndex);
            });
            return task;
        }
    }
}
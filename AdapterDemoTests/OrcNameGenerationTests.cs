﻿using AdapterDemo;
using AdapterDemo.Adapters;
using AdapterDemo.Generators;
using NUnit.Framework;

namespace AdapterDemoTests
{
    [TestFixture]
    public class OrcNameGenerationTests
    {
        [Test]
        public void GenerateName_Orc_Male_NoError()
        {
            var nameGenerator = new NameGenerator(new OrcNameGeneratorAdapter(new OrcNameGenerator()));

            var name = nameGenerator.GetMaleName();

            Assert.IsNotEmpty(name);
            StringAssert.Contains("gro-", name);
        }

        [Test]
        public void GenerateName_Orc_Female_NoError()
        {
            var nameGenerator = new NameGenerator(new OrcNameGeneratorAdapter(new OrcNameGenerator()));

            var name = nameGenerator.GetFemaleName();

            Assert.IsNotEmpty(name);
            StringAssert.Contains("gra-", name);
        }
    }
}
﻿using AdapterDemo;
using AdapterDemo.Adapters;
using AdapterDemo.Generators;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdapterDemoTests
{
    [TestFixture]
    public class BosmerNameGenerationTests
    {
        private List<string> maleFirstNameSuffixes = new List<string> { "cher", "dell", "driel", "gan", "gorn", "las", "man", "nis", "nor", "rim", "tan", "van" };
        private List<string> femaleFirstNameSuffixes = new List<string> { "dra", "fina", "gina", "landra", "lerva", "na", "nia", "sa", "sandra", "thia" };

        [Test]
        public void GenerateName_Orc_Male_NoError()
        {
            var nameGenerator = new NameGenerator(new BosmerNameGeneratorAdapter(new BosmerNameGenerator()));

            var name = nameGenerator.GetMaleName();

            Assert.IsNotEmpty(name);
            Assert.IsTrue(maleFirstNameSuffixes.Any(s => name.Contains(s)));
        }

        [Test]
        public void GenerateName_Orc_Female_NoError()
        {
            var nameGenerator = new NameGenerator(new BosmerNameGeneratorAdapter(new BosmerNameGenerator()));

            var name = nameGenerator.GetFemaleName();

            Assert.IsNotEmpty(name);
            Assert.IsTrue(femaleFirstNameSuffixes.Any(s => name.Contains(s)));
        }
    }
}
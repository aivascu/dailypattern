﻿#region

using System;

#endregion

namespace HorrorSoft.PLS
{
    public class PLSException : Exception
    {
        public PLSException()
        {
        }

        public PLSException(string message) : base(message)
        {
        }
    }
}
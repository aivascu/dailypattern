﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using HorrorSoft.PLS.Extensions;

namespace HorrorSoft.PLS
{
    public class PLSFile : ICollection<PLSEntry>
    {
        private readonly List<PLSEntry> _entries = new List<PLSEntry>();
        private int version;

        public PLSEntry this[int index] => _entries[index];

        public int NumberOfEntries => Count;

        public int Version => version;

        public PLSFile()
        {
            version = 2;
        }

        public void Load(string fileName, bool resolveRelativePaths = false)
        {
            _entries.Clear();

            using (var reader = new StreamReader(fileName))
            {
                string line;
                var lineCount = 0;

                PLSEntry entry = null;

                while ((line = reader.ReadLine()) != null)
                {
                    if (lineCount == 0)
                        if (string.CompareOrdinal(line, "[playlist]") != 0)
                            throw new PLSException("Winamp Playlist header is missing.");
                        else
                        {
                            lineCount++;
                            continue;
                        }

                    lineCount++;

                    if (line.StartsWith("File"))
                    {
                        var regex = new Regex(@"^(?:File)(?<index>\d+)(?:=)(?<path>.+)$");
                        var match = regex.Match(line);

                        if (!match.Success)
                            continue;

                        Uri path;
                        if (!Uri.TryCreate(match.Groups["path"].Value, UriKind.RelativeOrAbsolute, out path))
                            throw new PLSException("Invalid entry path.");


                        entry = new PLSEntry
                        {
                            FilePath = path
                        };

                        continue;
                    }

                    if (line.StartsWith("Length"))
                    {
                        var regex = new Regex(@"^(?:Length)(?<index>\d+)(?:=)(?<time>\d+)$");
                        var match = regex.Match(line);

                        if (!match.Success)
                            continue;

                        var time = match.Groups["time"].Value;

                        int seconds;
                        if (!int.TryParse(time, out seconds))
                            throw new PLSException("Invalid track duration.");

                        if (entry != null)
                            entry.Length = TimeSpan.FromSeconds(seconds);

                        continue;
                    }

                    if (line.StartsWith("Title"))
                    {
                        var regex = new Regex(@"^(?:Title)(?<index>\d+)(?:=)(?<title>.+)$");
                        var match = regex.Match(line);

                        if (!match.Success)
                            continue;

                        var title = match.Groups["title"].Value;

                        if (entry != null)
                            entry.FullTitle = title;

                        continue;
                    }

                    if (line.StartsWith("NumberOfEntries"))
                    {
                        var regex = new Regex(@"^(?:NumberOfEntries)(?:=)(?<count>\d+)$");
                        var match = regex.Match(line);

                        if (!match.Success)
                            continue;

                        var strCount = match.Groups["count"].Value;

                        int count;
                        if (!int.TryParse(strCount, out count) && count < 0)
                            throw new PLSException("Invalid number of entries");
                    }


                    if (line.StartsWith("Version"))
                    {
                        var regex = new Regex(@"^(?:Version)(?:=)(?<version>\d+)$");
                        var match = regex.Match(line);

                        if (!match.Success)
                            continue;

                        var strCount = match.Groups["version"].Value;

                        if (!int.TryParse(strCount, out version))
                            throw new PLSException("Invalid playlist version.");
                    }

                    if (entry != null)
                    {
                        _entries.Add(entry);
                    }

                    entry = null;
                }
            }
        }

        public void Save(string fileName, bool useAbsolutePaths = false, bool useLocalFilePath = true)
        {
            var workingUri = new Uri(Path.GetDirectoryName(fileName));

            using (var writer = new StreamWriter(fileName))
            {
                writer.WriteLine("[playlist]");

                for (var index = 0; index < Count; index++)
                {
                    var filePath = this[index].FilePath.ToString();

                    if (this[index].FilePath.IsFile && useLocalFilePath)
                        filePath = Uri.UnescapeDataString(this[index].FilePath.LocalPath);
                    else if (!this[index].FilePath.IsAbsoluteUri && useAbsolutePaths)
                        filePath = this[index].FilePath.MakeAbsoluteUri(workingUri).ToString();

                    writer.WriteLine($"File{index}={filePath}");
                    writer.WriteLine($"Length{index}={this[index].Length.TotalSeconds}");
                    writer.WriteLine($"FullTitle{index}={this[index].FullTitle}");
                }

                writer.WriteLine($"NumberOfEntries={NumberOfEntries}");
                writer.WriteLine($"Version={Version}");
            }
        }

        #region Implementation of IEnumerable

        public IEnumerator<PLSEntry> GetEnumerator()
        {
            return ((IEnumerable<PLSEntry>)_entries).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of ICollection<PLSEntry>

        public void Add(PLSEntry item)
        {
            _entries.Add(item);
        }

        public void Clear()
        {
            _entries.Clear();
        }

        public bool Contains(PLSEntry item)
        {
            return _entries.Contains(item);
        }

        public void CopyTo(PLSEntry[] array, int arrayIndex)
        {
            _entries.CopyTo(array, arrayIndex);
        }

        public bool Remove(PLSEntry item)
        {
            return _entries.Remove(item);
        }

        public int Count
        {
            get { return _entries.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        public PLSEntry Find(Predicate<PLSEntry> match)
        {
            return _entries.Find(match);
        }

        public List<PLSEntry> FindAll(Predicate<PLSEntry> match)
        {
            return _entries.FindAll(match);
        }
    }
}
﻿using System;

namespace HorrorSoft.PLS
{
    public class PLSEntry
    {
        public TimeSpan Length { get; set; }
        public string FullTitle { get; set; }
        public Uri FilePath { get; set; }
    }
}